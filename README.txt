Adafruit_NFCShield_I2C modified by Nick Stedman: original by adafruit, available here:
https://github.com/adafruit/Adafruit_NFCShield_I2C

Adds several data formatting methods.
Uses the Mifare Ultralight Write Method developed by mattoehrlein, available here:
https://github.com/mattoehrlein/Adafruit_NFCShield_I2C

—
original read me by adafruit

This is a library for the Adafruit PN532 NFC/RFID breakout boards
This library works with the Adafruit NFC breakout and shield
  ----> https://www.adafruit.com/products/364
  ----> https://www.adafruit.com/products/789
 
Check out the links above for our tutorials and wiring diagrams 
These chips use SPI or I2C to communicate

Adafruit invests time and resources providing this open source code, 
please support Adafruit and open-source hardware by purchasing 
products from Adafruit!

Written by Limor Fried/Ladyada & Kevin Townsend for Adafruit Industries.  
BSD license, check license.txt for more information
All text above must be included in any redistribution

To download: click the ZIP button above, rename
the uncompressed folder Adafruit_NFCShield_I2C. Check that the
Adafruit_NFCShield_I2C folder contains Adafruit_NFCShield_I2C.cpp and
Adafruit_NFCShield_I2C.h

Place the Adafruit_NFCShield_I2C library folder your
<arduinosketchfolder>/libraries/ folder. You may need to create the
libraries subfolder if its your first library. Restart the IDE.
